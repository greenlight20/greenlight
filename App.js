import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Alert, Image} from 'react-native';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {firstFloor: "Off",
                  secondFloor:"Off",
                  outlet1:"Off",
                  outlet2:"Off"};

  }
  componentDidMount() {
      var that = this;
    this.checkHardWareConnected(function(data){
      if(data==false){
        //console.log("ephraim");
        Alert.alert(
          'Warning',
          'Hardware not connected',
          [
            {text: 'OK', onPress: () => console.log('OK Pressed')},
          ],
          { cancelable: false }
        )
      }else{

        that.setButtonValue('D0', function(data){
          if(data=='["1"]'){
            that.setState({outlet1:"On"});
          }else{
            that.setState({outlet1:"Off"});
          }
        });
        that.setButtonValue('D2', function(data){
          if(data=='["1"]'){
            that.setState({outlet2:"On"});
          }else{
            that.setState({outlet2:"Off"});
          }
        });
        that.setButtonValue('D4', function(data){
          if(data=='["1"]'){
            that.setState({secondFloor:"On"});
          }else{
            that.setState({secondFloor:"Off"});
          }
        });
        that.setButtonValue('D5', function(data){
          if(data=='["1"]'){
            that.setState({firstFloor:"On"});
          }else{
            that.setState({firstFloor:"Off"});
          }
        });
        setInterval(function(){that.getPinValue();}, 20000);
      }
    });
  }
  toggleOutlet1(){
    if(this.state.outlet1=="Off"){
        this.togglePin('D0', '1', 'outlet1');
    }else{
        this.togglePin('D0', '0', 'outlet1');
    }
  }
  toggleOutlet2(){
    if(this.state.outlet2=="Off"){
        this.togglePin('D2', '1', 'outlet2');
    }else{
        this.togglePin('D2', '0', 'outlet2');
    }
  }
  secondFloor(){
    if(this.state.secondFloor=="Off"){
        this.togglePin('D4', '1', 'secondfloor');
    }else{
        this.togglePin('D4', '0', 'secondfloor');
    }
  }
  firstFloor(){
    if(this.state.firstFloor=="Off"){
        this.togglePin('D5', '1', 'firstfloor');
    }else{
        this.togglePin('D5', '0', 'firstfloor');
    }
  }
  setButtonValue(pin, callback){
    fetch('http://blynk-cloud.com/836268fbd00547a49fad3e89c4b8e297/get/'+pin, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      }
    }).then((responce)=>{
      //console.log(responce._bodyInit);
      callback(responce._bodyInit);
    });
  }
  getPinValue(){
    var that = this;
    this.setButtonValue('D0', function(data){
      if(data=='["1"]'){
        that.setState({outlet1:"On"});
      }else{
        that.setState({outlet1:"Off"});
      }
    });
    this.setButtonValue('D2', function(data){
      if(data=='["1"]'){
        that.setState({outlet2:"On"});
      }else{
        that.setState({outlet2:"Off"});
      }
    });
    this.setButtonValue('D4', function(data){
      if(data=='["1"]'){
        that.setState({secondFloor:"On"});
      }else{
        that.setState({secondFloor:"Off"});
      }
    });
    this.setButtonValue('D5', function(data){
      if(data=='["1"]'){
        that.setState({firstFloor:"On"});
      }else{
        that.setState({firstFloor:"Off"});
      }
    });
  }
  togglePin(pin, pinValue,myswitch){
    var that = this;
    that.checkHardWareConnected(function(data){
      if(data==false){
        Alert.alert(
          'Warning',
          'Hardware not connected',
          [
            {text: 'OK', onPress: () => console.log('OK Pressed')},
          ],
          { cancelable: false }
        )
      }else{
        fetch('http://blynk-cloud.com/836268fbd00547a49fad3e89c4b8e297/update/'+pin+'?value='+pinValue, {
          method: 'GET',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          }
        }).then((response) => {
          console.log(response);
          switch (myswitch) {
            case "firstfloor":
              if(pinValue==0){
                that.setState({
                  firstFloor:"Off"
                });
              }else{
                that.setState({
                  firstFloor:"On"
                });
              }

            break;
            case "secondfloor":
              if(pinValue==0){
                that.setState({
                  secondFloor:"Off"
                });
              }else{
                that.setState({
                  secondFloor:"On"
                });
              }

            break;
            case "outlet1":
              if(pinValue==0){
                that.setState({
                  outlet1:"Off"
                });
              }else{
                that.setState({
                  outlet1:"On"
                });
              }

            break;
            case "outlet2":
              if(pinValue==0){
                that.setState({
                  outlet2:"Off"
                });
              }else{
                that.setState({
                  outlet2:"On"
                });
              }

            break;
            default:

          }
        });
      }
    });

  }
  checkHardWareConnected(callback){
    fetch('http://blynk-cloud.com/836268fbd00547a49fad3e89c4b8e297/isHardwareConnected', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      }
    }).then((response)=>{
      console.log(response);
      if(response._bodyInit=="false"){
        callback(false);
      }else{
        callback(true);
      }
    }).catch((err)=>{
      callback(false);
      /*Alert.alert(
        'Warning',
        'Hardware not connected',
        [
          {text: 'OK', onPress: () => console.log('OK Pressed')},
        ],
        { cancelable: false }
      )*/
    });
  }
  render() {
    return (
      <View style={styles.container}>
          <View style={styles.rowContainer} >
            <View style={[styles.buttonContainer, styles.green]} >

                  <Text>First Floor: {this.state.firstFloor == 'Off' ? 'On' : 'Off'}</Text>
                  <TouchableOpacity
                      style={[styles.buttonUnPressed, (this.state.firstFloor == "Off") && styles.buttonPressed]}
                      onPress={this.firstFloor.bind(this)}
                      >
                      <Image
                        source={this.state.firstFloor == 'Off' ? require('./images/light_on.png') : require('./images/light_off.png')}
                      />
                  </TouchableOpacity>

            </View>
            <View style={[styles.buttonContainer, styles.blue]} >
                <Text>Second Floor: {this.state.secondFloor == 'Off' ? 'On' : 'Off'}</Text>
                <TouchableOpacity
                    style={[styles.buttonUnPressed, (this.state.secondFloor == "Off") && styles.buttonPressed]}
                    onPress={this.secondFloor.bind(this)}
                    >
                    <Image
                      source={this.state.secondFloor == 'Off' ? require('./images/light_on.png') : require('./images/light_off.png')}
                    />
                </TouchableOpacity>
            </View>
          </View>
          <View style={styles.rowContainer}>
            <View style={[styles.buttonContainer, styles.red]} >
                <Text>Outlet 1: {this.state.outlet1 == 'Off' ? 'On' : 'Off'}</Text>
                <TouchableOpacity
                    style={[styles.buttonUnPressed, (this.state.outlet1 == "Off") && styles.buttonPressed]}
                    onPress={this.toggleOutlet1.bind(this)}
                    >
                    <Image
                      source={this.state.outlet1 == 'Off' ? require('./images/light_on.png') : require('./images/light_off.png')}
                    />
                </TouchableOpacity>
            </View>
            <View style={[styles.buttonContainer, styles.yellow]} >
                <Text>Outlet 2: {this.state.outlet2 == 'Off' ? 'On' : 'Off'}</Text>
                <TouchableOpacity
                    style={[styles.buttonUnPressed, (this.state.outlet2 == "Off") && styles.buttonPressed]}
                    onPress={this.toggleOutlet2.bind(this)}
                    >
                    <Image
                      source={this.state.outlet2 == 'Off' ? require('./images/light_on.png') : require('./images/light_off.png')}
                    />
                </TouchableOpacity>
            </View>
          </View>
        </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  imgButton:{
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10
  },
  rowContainer:{
    flex: 1,
    flexDirection: 'row',
  },
  buttonContainer:{
    flex:1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  green:{
    backgroundColor:'#1abc9c',
  },
  blue:{
     backgroundColor:'#2980b9',
  },
  red:{
    backgroundColor:'#f1a9a0',
  },
  yellow:{
    backgroundColor:'#f1c40f',
  },
  myButton:{
    borderWidth:1,
    borderColor:'rgba(0,0,0,0.2)',
    alignItems:'center',
    justifyContent:'center',
    width:100,
    height:100,
    backgroundColor:'#fff',
    borderRadius:100,
    marginTop:20,
  },
  buttonPressed:{
    borderWidth:0,
    alignItems:'center',
    justifyContent:'center',
    width:128,
    height:128,
    borderRadius:100,
    marginTop:20,
    elevation   : 0,
  },
  buttonUnPressed:{
    borderWidth:0,
    alignItems:'center',
    justifyContent:'center',
    width:128,
    height:128,
    borderRadius:100,
    marginTop:20,
    elevation   : 20,

  },
  myText:{
    fontWeight: 'bold',
    fontSize: 30,
    color: 'white',
  },
});
